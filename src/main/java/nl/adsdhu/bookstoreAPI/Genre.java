package nl.adsdhu.bookstoreAPI;

import javax.persistence.*;

@Entity
@Table
public class Genre {

    @Id
    @SequenceGenerator(
            name = "genre_sequence",
            sequenceName = "genre_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "genre_sequence"
    )
    private Long id;
    private String name;
    public Genre() {}

    public Genre(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
