package nl.adsdhu.bookstoreAPI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/books")
public class BooksController {

    private final BooksService booksService;

    @Autowired
    public BooksController(BooksService booksService) {
        this.booksService = booksService;
    }

    @GetMapping
    public List<Book> getBooks() {
        return booksService.getBooks();
    }

    @PostMapping
    public void postBook(@RequestBody Book book) {
        booksService.postBook(book);
    }
}
